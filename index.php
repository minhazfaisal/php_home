<?php

   // array
$cars=["bmw","toyota","volvo"];
echo "I like ". $cars[0] ." , ". $cars[1] ." , ".$cars[2]. "<br/>";
   // associative $arrayName = array('' => , );
$age = ["faisal" =>"10","Ahmed" =>"20" ];
echo "faisal is" .$age['faisal']." years old. ". "<br/>";
   // loop through printing index array 
$cars=["BMW","Volvo","Bugatti","Aston Martin","Alfa Romeo","Audi"];
$arrlength=count($cars);

for ($i=0; $i < $arrlength ; $i++) { 
  echo $cars[$i];
  echo "<br/>";
}
   // loop through printing associative array
$age = ["Alfa" => "10","Ahmed" => "20","Faisal" => "30","Ali" => "40", ];

foreach ($age as $key => $value) {
  echo "name= " .$key. " and "."age= ".$value;
  echo "<br/>";
}
   // multidimention array 
$cars=[array("Volvo",100,96),array("BMW",60,59),array("Toyota",110,100)];

echo $cars[0][0].": Order: ".$cars[0][1].". Sold: ".$cars[0][2]."<br>";
echo $cars[1][0].": Order: ".$cars[1][1].". Sold: ".$cars[1][2]."<br>";
echo $cars[2][0].": Order: ".$cars[2][1].". Sold: ".$cars[2][2]."<br>";


?>

<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <title></title>
</head>
<body>

</body>
</html>

<?php

$color = "maroon";

$var = $color[2];

echo "$var";
echo "<br>";
?>

<?php

$num  = "1";
$num1 = "2";
      print $num+$num1;//The numbers inside the double quotes are considered as integers and not string, therefore the value 3 is printed and not 1+2.
      echo "<br>";

      $num  = "1";
      $num1 = "2";
      print $num ."+". $num1;

      echo "<br>";
      // global scope
      $a=5;
      function myfunction(){
        echo $a;
      }
      myfunction();
      echo $a;

      echo "<br>";
      $z=2;
      $x=50;
      echo $x."<br>";
      function afunction(){
        global $x,$z;
        $x=$z+$x;
      }
      afunction();
      echo $x."<br>";
      echo $b=$x+$z;

      echo "<br>";
      // static
      function myTest() {
        static $x = 0;
        echo $x;
        $x++;
      }
      myTest();
      echo "<br>";
      myTest();
      echo "<br>";
      myTest();

      echo "<br>";
      $foo = 'Bob';              
      $bar = &$foo;              
      $bar = "My name is $bar";  
      echo $bar;
      echo $foo;//The $bar = &$foo; line will reference $foo via $bar.

      echo "<br>";
      printf ('Hello World');

      echo "<br>";
      $color = "maroon";
      $var = $color[2];
      echo "$var";//:PHP treats strings in the same fashion as arrays, allowing for characters to be accessed via array offset notation.

      echo "<br>";
      $score = 1234;
      $scoreboard = (array) $score;
    echo $scoreboard[0];//:The (array) is a cast operator which is used for converting values from other data types to array.

    echo "<br>";
    $total = "25 students";
    $more = 10;
    $total = $total + $more;
    echo "$total";//The integer value at the beginning of the original $total string is used in the calculation. However if it begins with anything but a numerical value, the value will be 0.

    echo "<br>";
    $sub="students 25";
    $add=10;
    $plus=$sub+$add;
    echo $plus;

    echo "<br>";
    echo "\$x";//A backslash is used so that the dollar sign is treated as a normal string character rather than prompt PHP to treat $x as a variable. The backslash used in this manner is known as escape character.

    echo "<br>";
    $a = "clue";
    $a .= "get";
    echo "$a";//.= is a concatenation-assignment. $a equals its current value concatenated with “get”.

    echo "<br>";

    echo "<br>";

    echo "<br>";









































      ?>